import React from 'react';
import Home from './Cards';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<Home></Home>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});