import React from 'react';
import './css/plateau.css';
import { Link } from 'react-router-dom';

class Match extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      token: this.props.token,
      name: this.props.name,
      turn: false,
      autorize: true
    }
    this.attack = this.attack.bind(this);
    this.attackNexus = this.attackNexus.bind(this);
    this.poser = this.poser.bind(this);
    this.victime = this.victime.bind(this);
  }

  componentDidMount() {

    // DESIGN: Suppression des éléments qui gènent à la visualisation du match
    document.querySelector("footer").style.display = "None";
    document.querySelector("header").style.display = "None";
    document.querySelector("main").style.height = "100%";
  
    try {
      // Vérification des données du match (toutes les 2 secondes)
      this.matchState = setInterval(() => {
        fetch('https://league-of-stones.herokuapp.com/match/getMatch?token='+this.state.token)
        .then(result => result.json())
        .then((result)=>{

          // Récupération des informations du match
          this.setState({
            data: result.data
          })

          // Evenement: Quand l'un des deux joueurs gagnent
          if (this.state.data){
            if (this.state.data.status === "Player 1 won"){
              if(this.state.name === this.state.data.player1.name){
                fetch('https://league-of-stones.herokuapp.com/match/finishMatch?token='+this.state.token)
              }
              clearInterval(this.matchState);
            }
            if (this.state.data.status === "Player 2 won"){
              if(this.state.name === this.state.data.player2.name){
                fetch('https://league-of-stones.herokuapp.com/match/finishMatch?token='+this.state.token)
              }
              clearInterval(this.matchState);
            }

            // Lorsque c'est au tour du joueur, changez le turn à true.
            if (result.data.player1.name === this.state.name){
              if(result.data.player1.turn){
                this.setState({
                  turn: true
                })              
              }
              
              // Dans le cas contraire, mettre false.
              else{
                this.setState({
                  turn: false
                })
              }
            }
            else{
              // Lorsque c'est au tour du joueur, changez le turn à true.
              if(result.data.player2.turn){
                this.setState({
                  turn: true
                })              
              }
              // Dans le cas contraire, mettre false.
              else{
                this.setState({
                  turn: false
                })
              }
            }

            // Lorsque c'est au tour du joueur, executez une vérification des duels
            // (lorsque le joueur souhaite attaquer avec une carte)
            if (this.state.turn){
              this.verificationDuel();
            }
          }
        });
      }, 2000);
    } catch(e) {
      console.log(e);
    }
  }

  // Evenement: Piocher une carte
  pioche() {
    fetch('https://league-of-stones.herokuapp.com/match/pickCard?token='+this.state.token)
  }

  // Evenement: Poser une carte
  poser(champion) {
    if(this.state.data.player1.name === this.state.name && this.state.data.player1.board.length < 5) {
      fetch('https://league-of-stones.herokuapp.com/match/playCard?token='+this.state.token+'&card='+champion.key)
    }
    if(this.state.data.player2.name === this.state.name && this.state.data.player2.board.length < 5) {
      fetch('https://league-of-stones.herokuapp.com/match/playCard?token='+this.state.token+'&card='+champion.key)
    }
  }

  // Evenement: Attaquer avec une carte
  attack(champion) {
    // Verification: Tour du joueur + 1 attaque simultanément, pas plus! 
    if (this.state.turn && this.state.autorize){
      // Vérification: La carte n'a pas déjà attaqué?
      if (!champion.attack){
        if (this.state.duelAttaquant){
          const att = document.querySelector(".u"+this.state.duelAttaquant.key).classList;
          att && att.remove("card-attack");
        }
        this.setState({
          duelAttaquant: champion
        })
        const att1 = document.querySelector(".u"+champion.key).classList;
        att1 && att1.add("card-attack");
      }
    }
  }

  // Evenement: Attaquer une carte ennemie
  victime(champion){
    // Verification: Tour du joueur + 1 attaque simultanément, pas plus! 
    if (this.state.turn  && this.state.autorize){
      if (this.state.duelVictime){
        const vic1 = document.querySelector(".a"+this.state.duelVictime.key).classList;
        vic1 && vic1.remove("card-defense");
      }
        this.setState({
          duelVictime: champion
        })
      const vic1 = document.querySelector(".a"+champion.key).classList;
      vic1 && vic1.add("card-defense");
    }
  }

  // Evenement: Attaquer les vies du joueur ennemie
  attackNexus() {
    if(this.state.data.player1.name === this.state.name){
      // Vérification: le joueur ennemi a-t-il 0 carte dans le board?
      if (this.state.data.player2.board.length === 0){
        this.setState(
          {
            nexus: true
          }
        )
      }
    }
    else{
      // Vérification: le joueur ennemi a-t-il 0 carte dans le board?
      if (this.state.data.player1.board.length === 0){
        this.setState(
          {
            nexus: true
          }
        )
      }
    }
  }

  // Verification si le joueur souhaite utiliser une carte pour combattre
  verificationDuel(){
    const {duelAttaquant, nexus} = this.state

    // Verification: Le joueur souhaite un combat entre 1 carte...
    // ...et les vies ennemies?
    if (duelAttaquant && nexus){
      fetch('https://league-of-stones.herokuapp.com/match/attackplayer?token='+this.state.token+'&card='+duelAttaquant.key)
      this.setState(
        {
          nexus: null,
          duelAttaquant: null
        }
      )
    }

    // Verification: Le joueur souhaite un combat entre 1 carte...
    // ...et une carte ennemie?
    if (this.state.duelAttaquant && this.state.duelVictime){
      this.setState({
        autorize: false,
      })
      // Verification: l'attaque alliée doit être supérieure à l'armure ennemie
      if (this.state.duelAttaquant.stats.attackdamage > this.state.duelVictime.stats.armor) {
        const nodeAtk = document.querySelector(".u"+this.state.duelAttaquant.key)
        const nodeVict = document.querySelector(".a"+this.state.duelVictime.key)
        nodeAtk && nodeAtk.setAttribute("id", "attack-confirmed");
        nodeVict && nodeVict.setAttribute("id", "victim-confirmed");
        // Timer pour attendre que l'animation de l'attaque se termine
        setTimeout(() => {
          const {duelAttaquant, duelVictime} = this.state
          if (duelAttaquant && duelVictime){ 
              fetch('https://league-of-stones.herokuapp.com/match/attack?token='+this.state.token+'&card='+duelAttaquant.key+"&ennemyCard="+duelVictime.key)

              const nodeAtk = document.querySelector(".u"+duelAttaquant.key)
              if (nodeAtk){
                nodeAtk.classList.remove("card-attack");
                nodeAtk.setAttribute("id", "");
              }
             
              this.setState({
                duelAttaquant: null,
                duelVictime: null,
                autorize: true,
              })
          }
        }, 6500);
      }
      else{
        this.setState({
          duelAttaquant: null,
          duelVictime: null
        })
      }
    }
  }

  // Evenement: Passer son tour
  passer(){
    // Vérification: c'est le tour du joueur?
    if (this.state.turn){
      fetch('https://league-of-stones.herokuapp.com/match/endTurn?token='+this.state.token)
    }
  }


  render(){
    if( this.state.data ) {
      if( this.state.data.player1.turn || this.state.data.player2.turn) {
        let hand1 = null;
        let boardAlly = null;
        let boardEnemy = null;

        // Bouton: Passer le tour
        const passButton = <button className="passTurn" id={this.state.data.player1.cardPicked || this.state.data.player2.cardPicked ? "passTurn-invitation": null} onClick={() => this.passer()}>Terminer votre tour</button>;
        
        // Vérification: Qui est joueur1: l'ennemie, ou le gentil?
        if (Number.isInteger(this.state.data.player1.hand)){

          // Génération de la main du joueur
          hand1 = this.state.data.player2.hand.map((champion) => {
            const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
            return <article key={champion.key} id={champion.key} onClick={() => this.poser(champion)}><img src={url} alt="champimg"></img>
              <h4>{champion.name}</h4>
              <ul>
                <li className="attack">{champion.stats.attackdamage}</li>
                <li className="defence">{champion.stats.armor}</li>
              </ul>
            </article>
          })

          // Génération de la liste des cartes alliées
          boardAlly = this.state.data.player2.board.map((champion) => {
            const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
            return <article key={champion.key} className={champion.attack ? "attackedChamp": "u" + champion.key} onClick={() => this.attack(champion)}>
              <h4>{champion.name}</h4>
              <img src={url} alt="champimg"></img>
              <ul>
                <li className="attack">{champion.stats.attackdamage}</li>
                <li className="defence">{champion.stats.armor}</li>
              </ul>
            </article>
          })

          // Génération de la liste des cartes ennemies
          boardEnemy = this.state.data.player1.board.map((champion) => {
            const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
            return <article key={champion.key} className={"a" + champion.key} onClick={() => this.victime(champion)}>
              <h4>{champion.name}</h4>
              <img src={url} alt="champimg"></img>
              <ul>
                <li className="attack">{champion.stats.attackdamage}</li>
                <li className="defence">{champion.stats.armor}</li>
              </ul>
            </article>
          })
        }
        else{

          // Génération de la main du joueur
          hand1 = this.state.data.player1.hand.map((champion) => {
            const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
            return <article key={champion.key} id={champion.key} onClick={() => this.poser(champion)}><img src={url} alt="champimg"></img>
              <h4>{champion.name}</h4>
              <ul>
                <li className="attack">{champion.stats.attackdamage}</li>
                <li className="defence">{champion.stats.armor}</li>
              </ul>
            </article>
          })

          // Génération de la liste des cartes alliées
          boardAlly = this.state.data.player1.board.map((champion) => {
            const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
            return <article key={champion.key} className={champion.attack ? "attackedChamp": "u" + champion.key} onClick={() => this.attack(champion)}>
              <h4>{champion.name}</h4>
              <img src={url} alt="champimg"></img>
              <ul>
                <li className="attack">{champion.stats.attackdamage}</li>
                <li className="defence">{champion.stats.armor}</li>
              </ul>
            </article>
          })

          // Génération de la liste des cartes ennemies
          boardEnemy = this.state.data.player2.board.map((champion) => {
            const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
            return <article key={champion.key} className={"a" + champion.key} onClick={() => this.victime(champion)}>
              <h4>{champion.name}</h4>
              <img src={url} alt="champimg"></img>
              <ul>
                <li className="attack">{champion.stats.attackdamage}</li>
                <li className="defence">{champion.stats.armor}</li>
              </ul>
            </article>
          })
        }

        // Boutton: Piocher
        const Pioche = () => {
          if (Number.isInteger(this.state.data.player1.hand)){
            if (this.state.data.player2.turn){
              if(this.state.data.player2.cardPicked){
                return <button id="pioche-button-f"> Vous avez déjà pioché</button>
              }
              return <button id="pioche-button" onClick={() => this.pioche()}> Piocher</button>
            }
            else{
              return <button id="pioche-button-o">Tour de l'adversaire</button>
            }
          }
          else{
            if (this.state.data.player1.turn){
              if(this.state.data.player1.cardPicked){
                return <button id="pioche-button-f"> Vous avez déjà pioché</button>
              }
              return <button id="pioche-button" onClick={() => this.pioche()}> Piocher</button>
            }
            else{
              return <button id="pioche-button-o">Tour de l'adversaire</button>
            }
          }
        }

        // Génération de la main ennemie
        const CartesEnemy = () => {
          let cards = []
          if (Number.isInteger(this.state.data.player1.hand)){
            for (let i=1; i <= this.state.data.player1.hand; i++){
              if (i === this.state.data.player1.hand-1){
                cards.push(<img key={i} id={"cardsemi"} src="images/game/card.jpg" alt="carte cachee"></img>);
              }
              else if (i === this.state.data.player1.hand){
                cards.push(<img key={i} id={"cardlast"} src="images/game/card.jpg" alt="carte cachee"></img>);
              }
              else{
                cards.push(<img key={i} className={"card"+ i} src="images/game/card.jpg" alt="carte cachee"></img>);
              }
            }
          }
          else{
            for (let i=1; i <= this.state.data.player2.hand; i++){
              if (i === this.state.data.player2.hand-1){
                cards.push(<img key={i} id={"cardsemi"} src="images/game/card.jpg" alt="carte cachee"></img>);
              }
              else if (i === this.state.data.player2.hand){
                cards.push(<img key={i} id={"cardlast"} src="images/game/card.jpg" alt="carte cachee"></img>);
              }
              else{
                cards.push(<img key={i} className={"card"+ i} src="images/game/card.jpg" alt="carte cachee"></img>);
              }
            }
          }
          return <article id="main_joueur1" className="mains">{cards}</article>
        }
        return <section id="plateau">
        <aside id="infoj">
        { this.state.turn ? passButton : null}
          <article id="joueur1" className="logo" onClick={() => this.attackNexus()}>
            <div id="nexus1">
              <p>{ this.state.data.player2.name === this.state.name ? this.state.data.player1.hp : this.state.data.player2.hp}</p>
            </div>
            <h2>{ this.state.data.player2.name === this.state.name ? this.state.data.player1.name : this.state.data.player2.name}</h2>
            <img src="images/avatar/00.jpg" alt="logo joueur"></img>
          </article>
          <CartesEnemy />
          <article id="joueur2" className="logo">
            <img src="images/avatar/00.jpg" alt="logo joueur"></img>
            <h2>{ this.state.data.player1.name === this.state.name ? this.state.data.player1.name : this.state.data.player2.name}</h2>
            <div id="nexus2">
              <p>{ this.state.data.player1.name === this.state.name ? this.state.data.player1.hp : this.state.data.player2.hp}</p> 
            </div>
          </article>
          <article id="main_joueur2" className="mains">
            {hand1}
          </article>
          <Pioche />
        </aside>
        <section id="place">
          <article id="choice">
              <section id="cards-top" className="cards">
                {boardEnemy}
              </section>
              <section id="cards-bottom" className="cards">
                {boardAlly}
              </section>
          </article>
        </section>
          <section id="fixed_welcome">
            <img src="images/logo.png" alt=""></img>
          </section>
          <section id="fixed_welcome_2">
            <p>Lancement de la partie...</p>
          </section>
          <img id="flame1" src="images/flame.gif" alt=""></img>
          <img id="flame2" src="images/flame.gif" alt=""></img>
          <img id="flame3" src="images/flame.gif" alt=""></img>
          <img id="flame4" src="images/flame.gif" alt=""></img>
      </section>
      }
      else{
        if (this.state.data.player1.hp < 0){
          document.querySelector("footer").style.display = "block";
          document.querySelector("header").style.display = "block";
          if (this.state.data.player1.name === this.state.name){
            return <p id="loseboy">{this.state.data.player1.name} a gagné la partie! Vous devez acheter un kebab à votre adversaire.
            <img alt="gif" src="images/gif/16.gif"></img>
            <Link to="./" id="endRedirect">Retour à l'accueil...</Link>
            </p>
          }
          else{
            return <p id="winboy">Vous avez gagné la partie, la victime vous doit un bon kebab ;)
            <img alt="gif" src="images/gif/1.gif"></img>
            <Link to="./" id="endRedirect">Retour à l'accueil...</Link></p>
          }
        }
        
        else if (this.state.data.player2.hp < 0){
          document.querySelector("footer").style.display = "block";
          document.querySelector("header").style.display = "block";
          if (this.state.data.player2.name === this.state.name){
            return <div><p id="loseboy">{this.state.data.player1.name} a gagné la partie! Vous devez acheter un kebab à votre adversaire.
            <img alt="gif" src="images/gif/16.gif"></img>
            <Link to="./" id="endRedirect">Retour à l'accueil...</Link></p>
            <div id="normal_bg"></div></div>
          }
          else{
            return <div><p id="winboy">Vous avez gagné la partie, la victime vous doit un bon kebab ;)
            <img alt="gif" src="images/gif/1.gif"></img>
            <Link to="./" id="endRedirect">Retour à l'accueil...</Link></p>
            <div id="normal_bg"></div></div>
          }
        }
        else{ 
          return <div><p id="waiting_game">Attente du second joueur...
          <img src="images/gif/sleep.gif" alt="poro qui dort"></img>
          </p>
                <div id="normal_bg"></div></div>
        }
      }
    }
    return null
  }
  
}
export default Match;