import React from 'react';
import Presentation from './Presentation';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<Presentation></Presentation>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});