import React from 'react';
import './home.css';
class Presentation extends React.Component{
  render(){
    return <section id="home">
      <article>
        <h2>Presentation du projet</h2>
        <p>Ce projet a commencé bien avant que nous soyons dans l'Université. Il s'agit d'un projet pour le module <b>MIC0601V - Informatique 3</b> de la licence MIASHS.<br></br>
        Il s'agit d'un mashup (mélange) de deux jeux vidéos. En utilisant le système de jeu de Hearthstone (HS) développé par Blizzard™ nous avons utilisé les données ouvertes du jeu League Of Legends (LoL) développé par Riot Games™.<br></br>
        <img src="images/fusion.png" alt=""></img>
        <br></br>Ce projet s’intitule "League of Stones".<br></br>
        Riot Games™ (l’entreprise qui a créé LoL) a mis à disposition une API permettant de récupérer les données du jeu, notamment les données concernant les champions: c'est ce que nous utilisons.<br></br>
        Une présentation détaillée du jeu Hearthstone est disponible ici : https ://playhearthstone.com/frfr/game-guide/. A la place des cartes de Blizzard, vous allez avoir des cartes des champions de LoL.<br></br>Le reste du jeu se base sur les même concepts que le jeu Hearthstone.
        </p>
      </article>
      <div id="normal_bg"></div>
      </section>
  }
  
}
export default Presentation;