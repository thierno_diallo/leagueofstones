import React from 'react';
import './form.css';
import { Form, Field } from 'react-final-form';
import { Link } from 'react-router-dom';

class Register extends React.Component{
  render() {
    const onSubmit = (pFormValuesObj)=>{
      if (pFormValuesObj.password !== pFormValuesObj.passwordc) {
        msg = "les mots de passe ne correspondent pas"
      }else{
        const lInit = {
          method: 'GET',
          headers: {
          'Content-type': 'application/json; charset=UTF-8'
          }
        }
        fetch("https://league-of-stones.herokuapp.com/users/subscribe?email="+pFormValuesObj.email+"&name="+pFormValuesObj.firstName+"&password="+pFormValuesObj.password+"", lInit)
        .then(res => res.json()).then((response) => 
        {
          document.querySelector(".forms > form").remove();
          var ele = document.querySelector(".forms");
          ele.innerHTML += '<ul></ul>';
          var ul = document.querySelector(".forms ul");
          if(response.status === 'ok'){
            ul.innerHTML += '<li><img src="images/logo.png"></img></li>';
            ul.innerHTML += '<li><p id="valid">Creation du compte effectuee</p></li>';
          }else{
            msg = response.message
            ul.innerHTML += '<li><img src="images/logo.png"></img></li>';
            ul.innerHTML += "<li><p id='valid'>"+msg+"</p></li>";
          }
        })
      }
    }

    var msg = ""
    return <section className="forms">
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit, form}) => (
        <form onSubmit={handleSubmit}>
          <ul>
            <li><img src="images/logo.png" alt="logo"></img></li>
            <li>{msg}</li>
            <li>
              <Field name="email" component="input" type="mail" placeholder="Adresse Mail" required/>
            </li>
            <li>
              <Field name="firstName" component="input" type="text" placeholder="Prenom" required/>
            </li>
            <li>
              <Field name="password" component="input" type="password" placeholder="Mot de passe" required/>
            </li>
            <li>
              <Field name="passwordc" component="input" type="password" placeholder="Confirmation du MDP" required/>
            </li>
            <li>
              <button type="submit">S'inscrire</button>
              <button onClick={form.reset} type="button">Renitialiser</button>
            </li>
            <li><Link to="/Login">Déjà un compte? Cliquez-ici</Link></li>
          </ul>
        </form>
        
      )} 
    />
    <div id="normal_bg"></div>
  </section>
  }
}
export default Register;