import React from 'react';
import { Link } from 'react-router-dom';
import './home.css';
class Home extends React.Component{
  render(){
    return <section id="homepage" className="los_home">
            <Link id="los_home_pres" to="/presentation">
            <article>
              <p>Presentation du projet</p>
            </article>   
            </Link>
            <Link id="los_home_propos" to="/about">
            <article>
              <p>A propos</p>
            </article>   
            </Link>
            <Link id="los_home_cards" to="/cards">
            <article>
              <p>Les cartes</p>
            </article>   
            </Link>
            <Link id="los_home_tuto" to="/tutoriel">
            <article>
              <p>Comment jouer?</p>
            </article>   
            </Link>
          <div id="normal_bg"></div>
      </section>
  }
  
}
export default Home;