import React from 'react';
import './form.css';
import { Form, Field } from 'react-final-form'
import { Redirect } from 'react-router';

class Profil extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
        token: this.props.token,
        email: this.props.email,
        name: this.props.name,
        redirection: false
    }
  }
  render() {
    const onSubmit = (pFormValuesObj)=>{
      const lInit = {
        method: 'GET',
        headers: {
        'Content-type': 'application/json; charset=UTF-8',
        }
      }
      fetch("https://league-of-stones.herokuapp.com/users/unsubscribe?token="+this.state.token+"&email="+this.state.email+"&password="+pFormValuesObj.password+"", lInit)
      .then(res => res.json()).then((response) => 
      {
        if(response.status === 'ok'){
          this.props.isDeconnected();
          this.setState({
            redirection: true
          })
        }else{
          msg = "Mot de passe incorrect"
        }
      })
    }
    if (this.state.redirection){
      return <Redirect to='/'/>;
    }
    var msg=""
    return <section id="profil" className="forms">
      <Form id="formulaire_unsuscribe"
      onSubmit={onSubmit}
      render={({ handleSubmit, form}) => (
        <form onSubmit={handleSubmit}>
          <h2>Votre profil</h2>
          <ul>
            <li>Adresse mail: {this.state.email}</li>
            <li>Nom d'utilisateur: {this.state.name}</li>
            <li>
              <Field name="password" component="input" type="password" placeholder="Mot de passe" />
            </li>
            <li>
              <button type="submit">Se désinscrire</button>
            </li>
            <p>{msg}</p>
            <p id="red">Attention: Cette action est irréversible!</p>
          </ul>
        </form>
      )} 
    />
    <div id="normal_bg"></div>
   </section>
  }
}
export default Profil;