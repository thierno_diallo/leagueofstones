import React from 'react';
import './home.css';
class Home extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      data: [],  //list of all the cards of the games
      deck: [],  //list of the card pick in the deck
    }
  }
  componentDidMount() {
    fetch('https://league-of-stones.herokuapp.com/cards/getAll') //request of the cards with there data
   .then(result => result.json())
   .then((result)=>{
     this.setState({
       data: result.data //store the cards in data
     })
  });
  }
  render() {
    const { data } = this.state
    const result = data.map((champion) => { 
      const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg' //link of the champions image
      return <article key={champion.name}>
                <h2>{champion.name}</h2>
                <h4>{champion.title}</h4>
                <ul>
                  <li className="attack">{champion.stats.attackdamage}</li>
                  <li className="defence">{champion.stats.armor}</li>
                </ul>
                <img src={url} alt={champion.key}></img>
             </article>
    })
    return <section className="los_home">
        {result};
      <div id="normal_bg"></div>
      </section>
  }
  
}
export default Home;