import React from 'react';
import { Redirect } from 'react-router-dom';
class MatchMaking extends React.Component{
  _isMounted = false;
  constructor(props){
    super(props)
    this.state = {
      invite: [],
      joueurs: [],
      token: this.props.token,
      name: this.props.name,
      matchmaking: true
    }
    this.acceptInvit = this.acceptInvit.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  // Evenement: Demander une invitation à un joueur
  handleClick(name, matchmaking) {
    if (name && matchmaking){
      fetch('https://league-of-stones.herokuapp.com/matchmaking/request?token='+this.state.token+"&matchmakingId="+matchmaking)
      .then(result => result.json())
      .then((result)=>{
        if (result.status === 'ok'){
          const cssfind = "#j" + name + " button";
          const cssfind2 = "#j" + name;
          document.querySelector(cssfind).remove();
          document.querySelector(cssfind2).textContent +=" - Demande envoyée";
        }
      });
    }
  }

  // Evenement: Accepter l'invitation d'un joueur
  acceptInvit(name, matchmaking) {
    if (matchmaking && name){
      fetch('https://league-of-stones.herokuapp.com/matchmaking/acceptRequest?token='+this.state.token +"&matchmakingId="+matchmaking)
      .then(result => result.json())
      .then((result)=>{
        if (result.status === 'ok'){
          const cssfind = "#i" + name + " button";
          const cssfind2 = "#i" + name;
          document.querySelector(cssfind).remove();
          document.querySelector(cssfind2).textContent +=" - Le combat va commencer!";
          fetch('https://league-of-stones.herokuapp.com/match/getMatch?token='+this.state.token)
        }
      });
    }
  }

  // Vérifications régulières de la liste des joueurs en recherche de match
  async componentDidMount() {
    if (this.state.matchmaking){
      this._isMounted = true;
      try {
        this.roue = setInterval(() => {
          fetch('https://league-of-stones.herokuapp.com/matchmaking/participate?token='+this.state.token)
          .then(result => result.json())
          .then((result)=>{
            if(this.state.token){
              if(result.data.match){
                if(result.data.match.player1.name === this.state.name || result.data.match.player2.name === this.state.name){
                  this.setState({
                    redirect: true
                  })
                    // Suppression de l'intervalle lorsque 2 joueurs générent un match
                  clearInterval(this.roue);
                }
              }
              if (this._isMounted) {
                this.setState({
                  invite: result.data.request
                })
              }
            }
          });
          fetch('https://league-of-stones.herokuapp.com/matchmaking/getAll?token='+this.state.token)
          .then(result => result.json())
          .then((result)=>{
            if (this._isMounted) {
              this.setState({
                joueurs: result.data
              })   
            }   
          });
        }, 3000);
      } catch(e) {
        console.log(e);
      }
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  render(){
    // Action lorsque les deux joueurs s'acceptent
    if(this.state.redirect){
      return <Redirect to='/Deck'/>
    }

    let dispos = 0;
    let invitations = 0;
  
    // Vérification si le joueur est connecté
    if(this.state.token){
      // Liste des joueurs disponibles (pour inviter)
      dispos = this.state.joueurs.map((joueur) => {
        const joueurcss = 'j' + joueur.name;
        return <li id={joueurcss} key={joueur.name}>{joueur.name}<button onClick={() => this.handleClick(joueur.name, joueur.matchmakingId)} className="fa-paper-plane"> Defier</button></li>
      })

      // Liste des demandes d'invitations (pour accepter)
      invitations = this.state.invite.map((joueur) => {
        const joueurcss = 'i' + joueur.name;
        return <li id={joueurcss} key={joueur.name}>{joueur.name}<aside><button onClick={() => this.acceptInvit(joueur.name, joueur.matchmakingId)} className="fa-check"> Accepter</button></aside></li>
      })
    }
    // Si le joueur n'est pas connecté, le rediriger à la page de connexion
    else{
      return <Redirect to='/login'/>;
    }
  
  // Affichage générale de la page avec les composants dispos + invitations
    return <section id="matchmaking-list">
      <article id="invite">
        <h2>Invitations recues</h2>
        <p>Vous avez des invitations? Acceptez-les pour jouer!</p>
        <ul>
          {invitations}
        </ul>
      </article>
      <article id="joueurs">
        <h2>Joueurs disponibles</h2>
        <p>Des joueurs sont connectés? Invitez-les au combat!</p>
        <ul>
          {dispos}
        </ul>
      </article>
      <div id="normal_bg"></div>
    </section> 
  }
}
export default MatchMaking;