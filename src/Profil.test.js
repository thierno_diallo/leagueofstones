import React from 'react';
import Profil from './Profil';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<Profil></Profil>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});