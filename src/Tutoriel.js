import React from 'react';
import './home.css';
class About extends React.Component{
  render(){
    return <section id="home">
      <article>
        <h2>Tutoriel: Comment jouer?</h2>
        <section>
          <h3>Etape 1: Création du compte</h3>
          <p>Pour jouer, commencez par vous inscrire.</p>
        </section>
        <section>
          <h3>Etape 2: Connexion au compte</h3>
          <p>Une fois que votre compte est crée, il est maintenant temps de vous connecter! :)</p>
        </section>
        <section>
          <h3>Etape 3: Préparez votre deck</h3>
          <p></p>
        </section>
      </article>
      <div id="normal_bg"></div>
      </section>
  }
  
}
export default About;