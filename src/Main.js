import React from 'react';
import Home from './Home';
import Deck from './Deck';
import Match from './Match';
import MatchMaking from './MatchMaking';
import Register from './Register';
import Login from './Login';
import File from './File';
import Profil from './Profil';
import Cards from './Cards';
import About from './About';
import Tutoriel from './Tutoriel';
import Presentation from './Presentation';
import Deconnexion from './Deconnexion';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default class LeagueStone extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isConnected: false,
      token: 0
    }
  }

  changeConnected(data) {
    this.setState({
        isConnected: true,
        token: data.token,
        name: data.name,
        id: data.id,
        email: data.email
      })
  }
  changeDeconnected() {
    this.setState({
        isConnected: false
      })
  }

  render(){
    let myDiv, myDivMobile;
    if(this.state.isConnected){
      myDiv =<div id="right">
        <Link to="./profil" id="profil-button" className="deco-button">Profil</Link>
        <Link to="./deconnexion" id="deconnexion-button" className="deco-button">Deconnexion</Link>
      </div>
      myDivMobile = <section>
        <Link to="./profil" id="profil-button" className="deco-button">Profil</Link>
        <Link to="./deconnexion" id="deconnexion-button" className="deco-button">Deconnexion</Link>
      </section>
    } else{
      myDiv =<div id="right">
          <Link to="./login" id="login-button" className="deco-button">Connexion</Link>
          <Link to="./register" id="register-button" className="deco-button">Inscription</Link>
        </div>
      myDivMobile = <section>
      <Link to="./login" id="login-button" className="deco-button">Connexion</Link>
      <Link to="./register" id="register-button" className="deco-button">Inscription</Link>
    </section>
    }

    return (
      <Router>
        <header>
          <nav>
            <Link to="./"><img src="images/logo.png" alt="logo du jeu"></img></Link>
            <Link to={'./file'} id="play-button" className="nav-link" >Jouer</Link>
            <Link to="./" id="home-button" className="nav-link" >Accueil</Link>
            {myDiv}
          </nav>
          <nav id="mobile">
            <Link to="./"><img src="images/logo.png" alt="logo du jeu"></img></Link>
            <Link to={'./file'} id="play-button" className="nav-link" >Jouer</Link>
            <aside>
              <p className="fa fa-bars"></p>
              {myDivMobile}            
            </aside>


          </nav>
        </header>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/Deck">
                <Deck token={this.state.token} />
              </Route>
              <Route path="/file">
                <File />
              </Route>
              <Route exact path="/Match">
                <Match token={this.state.token} name={this.state.name} />
              </Route>
              <Route exact path="/MatchMaking">
                <MatchMaking token={this.state.token} name={this.state.name}/>
              </Route>
              <Route exact path="/about">
                <About />
              </Route>
              <Route exact path="/login">
                <Login isConnected={this.changeConnected.bind(this)}
                />
              </Route>
              <Route exact path="/deconnexion">
                <Deconnexion token={this.state.token} isDeconnected={this.changeDeconnected.bind(this)}
                />
              </Route>
              <Route exact path="/profil">
                <Profil token={this.state.token} email={this.state.email} name={this.state.name} isDeconnected={this.changeDeconnected.bind(this)}/>
              <Route/>
              </Route>
              <Route exact path="/register">
                <Register />
              </Route>
              <Route exact path="/cards">
                <Cards />
              </Route>
              <Route exact path="/tutoriel">
                <Tutoriel />
              </Route>
              <Route exact path="/presentation">
                <Presentation />
              </Route>
      </Router>
      );
  }
}