import React from 'react';
import './home.css';
class About extends React.Component{
  render(){
    return <section id="home">
      <article>
        <h2>A propos</h2>
        <section>
          <h3>Inspirations</h3>
          <p>Nous utilisons les principes et le contenu de jeux vidéos, nous souhaitons donc les créditer.</p>
          <ul>
            <li>
              <img src="https://euw.leagueoflegends.com/profiles/lol2_profile/themes/custom/lolt/img/lol_logo.png" alt="lol_logo"></img>
              League of Legends (abrégé LoL), anciennement nommé League of Legends: Clash of Fates est un jeu vidéo de type arène de bataille en ligne (MOBA) gratuit développé et édité par Riot Games sur Windows et Mac OS X1.<br></br>
              Le jeu a été évoqué pour la première fois le 7 octobre 2008 et est entré en phase bêta le 10 avril 2009 pour finalement sortir au grand public le 27 octobre 2009.<br></br>
              Dans League of Legends, le joueur contrôle un champion aux compétences uniques dont la puissance augmente au fil de la partie se battant contre une équipe de joueurs en temps réel la plupart du temps.<br></br>
              L'objectif d'une partie est, dans la quasi-totalité des modes de jeu, de détruire le "Nexus" ennemi, bâtiment situé au cœur de la base adverse protégé par des tourelles et inhibiteurs.<br></br>
              Le jeu comporte un grand nombre de similitudes avec Defense of the Ancients de par le fait que la majorité des premiers développeurs de League of Legends n'étaient autres que les créateurs de DotA.
              <br></br><i>(Source: Wikipédia)</i>
            </li>
            <li>
              <img src="https://d2q63o9r0h0ohi.cloudfront.net/images/logos/navigation/logo-bf01817953be4b653a29e5766a3eb7f72f65d0a77ac5801d8a13b9f605362ec756b3da7eb749d5d30c0a1f8c577d2738b20ae0e78f828ed3984873cf78997637.png"  alt="hs_logo"></img>
              Hearthstone (anciennement Hearthstone: Heroes of Warcraft) est un jeu de cartes à collectionner en ligne, développé et édité par la société Blizzard Entertainment. C'est un jeu gratuit (free to play) s'inspirant de l'univers de fiction médiéval-fantastique du jeu vidéo Warcraft développé par Blizzard.<br></br>
              Annoncé le 22 mars 20132, le jeu sort officiellement le 11 mars 2014 sur Windows et OS X. La version pour tablette iPad sort le 17 avril 2014, celle pour Android le 16 décembre 2014 et celle pour iPhone et smartphones Android le 15 avril 2015.<br></br>
              Connaissant un grand succès à sa sortie, le jeu remporte le prix du meilleur jeu portable lors des Game Awards 20143.<br></br>
              À la date de novembre 2018, Blizzard annonce avoir atteint le cap des 100 millions de joueurs inscrits au jeu.
              <br></br><i>(Source: Wikipédia)</i>
            </li>
          </ul>
        </section>
        <section>
          <h3>L'Equipe</h3>
          <p>L'équipe qui développe cette version de League of Stones est composée de cinq membres.</p>
          <ul>
            <li>Baptiste Lapeyre - Chef de projet</li>
            <li>Fatima Al Mughrabi - Responsable de qualité</li>
            <li>Thierno Diallo - Architecte</li>
            <li>Anthony Lieures - Responsable Mobile (Responsive)</li>
            <li>Thomas Pazder - Designer</li>
          </ul>
          <p>Note: Tous les membres de l'équipe ont participé à la réalisation de ce projet en termes de codes, en plus des responsabilités ci-dessus.</p>
        </section>
        <section>

        </section>
      </article>
      <div id="normal_bg"></div>
      </section>
  }
  
}
export default About;