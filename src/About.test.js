import React from 'react';
import About from './About';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<About></About>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});