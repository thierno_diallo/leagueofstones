import React from 'react';
import LeagueStone from './Main';
import MatchMaking from './MatchMaking';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<LeagueStone></LeagueStone>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});