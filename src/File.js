import React from 'react';
import { Link } from 'react-router-dom';
class File extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      redirection: false,
      token : this.props.token
    }
  }
  render() {
    return <section><section id="debut-partie" style={{display: "block"}}>
        <article>
          <h3>Souhaitez-vous jouer et entrez dans la legende?</h3>          
          <aside>
            <Link to="/MatchMaking">Trouvez un adversaire</Link>
            <Link to="/">Fuir comme un lache !</Link>
          </aside> 
        </article>
    </section>
    </section>
  }
}
export default File;
