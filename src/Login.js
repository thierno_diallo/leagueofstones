import React from 'react';
import './form.css';
import { Form, Field } from 'react-final-form';
import { Link } from 'react-router-dom';

class Login extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      token: null,
      name: null,
      email: null,
      isConnected: false
    }
  }
  render() {
    const onSubmit = (pFormValuesObj)=>{
      const lInit = {
        method: 'GET',
        headers: {
        'Content-type': 'application/json; charset=UTF-8'
        }
      }
      fetch("https://league-of-stones.herokuapp.com/users/connect?email="+pFormValuesObj.email+"&password="+pFormValuesObj.password+"", lInit)
      .then(res => res.json()).then((response) => 
      {
        if(response.status === 'ok'){
          this.setState({
            token: response.data.token,
            name: response.data.name,
            id: response.data.id,
            email: response.data.email,
            isConnected: true
          })
          this.props.isConnected(response.data);
          document.querySelector(".forms > form").remove();
          var ele = document.querySelector(".forms");
          ele.innerHTML += '<ul></ul>';
          var ul = document.querySelector(".forms ul");
          ul.innerHTML += '<li><img src="images/logo.png"></img></li>';
          ul.innerHTML += "<li><p id=\"valid\">Vous etes maintenant connecte,"+this.state.name+"</p></li>";
        }else{
          msg = response.message
        }
      })
    }
    var msg=""
    return <section className="forms">
      <Form
      onSubmit={onSubmit}
      render={({ handleSubmit, form}) => (
        <form onSubmit={handleSubmit}>
          <ul>
            <li><img src="images/logo.png" alt=""></img></li>
            <p>{msg}</p>
            <li>
            <Field name="email" component="input" type="text" placeholder="Adresse Mail"/>
          </li>
            <li>
              <Field name="password" component="input" type="password" placeholder="Mot de passe" />
            </li>
            <li>
              <button type="submit">Se connecter</button>
            </li>
            <li><Link to="/Register">Pas de compte? S'inscrire ici</Link></li>
          </ul>
        </form>
      )} 
    />
    <div id="normal_bg"></div>
   </section>
  }
}
export default Login;