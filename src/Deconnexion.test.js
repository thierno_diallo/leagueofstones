import React from 'react';
import Deconnexion from './Deconnexion';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<Deconnexion></Deconnexion>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});