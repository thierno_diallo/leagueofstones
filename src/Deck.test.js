import React from 'react';
import Deck from './Deck';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<Deck></Deck>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});