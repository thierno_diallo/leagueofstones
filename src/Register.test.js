import React from 'react';
import Register from './Register';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const tree = renderer.create(<Register></Register>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});