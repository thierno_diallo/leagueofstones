import React from 'react';
import './form.css';
import { Redirect } from 'react-router';


class Deconnexion extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            redirection: false, //variable use to redirect the user to the home page of the game when he clicked on disconnect
            token: this.props.token // token of connection
        }
    }
    componentDidMount() {
        // Suppression de toutes les intervalles: pour être sur qu'il ne reste rien!
        for(let i = 0; i < 100; i++) {
            window.clearInterval(i);
        }
        const lInit = {
            method: 'GET',    
        }
        fetch("https://league-of-stones.herokuapp.com/users/disconnect?token="+this.state.token, lInit) // request for the disconnection
        .then(res => res.json()).then((response) => 
        {
            this.props.isDeconnected();
            this.setState({
                redirection: true
            })
        })
    }

  render() {
    if (this.state.redirection){
        return <Redirect to='/'/>; // redirect the user to the home page of the game
    }
    return <section className="forms"> 
        <p>Déconnexion en cours, veuillez patienter...</p>
    <div id="normal_bg"></div>
   </section>
  }
}
export default Deconnexion;