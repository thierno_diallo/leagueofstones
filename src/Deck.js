import React from 'react';
import './play.css';
import {Redirect} from 'react-router-dom';
class Deck extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      data: [],  //list of all the cards of the games
      deck: [],  //list of the card pick in the deck
      token: this.props.token, // token of the connection
    }
    this.nombre = 0; //number of card selected in the deck
    this.validate = 0;
    this.validateDeck = this.validateDeck.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this._isMounted = false;

  }
  validateDeck() { // triggered when the user have finished the selection of his deck
    this.validate = 1;
    const { data } = this.state
    const deck2 = [];
    data.map((champion) => { //for each card of the game if is select by the user it is put in deck2
      if (champion.statut === 1){
        deck2.push({"key": champion.key});
      }
      return"";
    })
    const lInit = {
      method: 'GET',    
    }
    fetch("https://league-of-stones.herokuapp.com/match/initDeck?token="+this.state.token+"&deck="+JSON.stringify(deck2), lInit) //notify the server and send the deck 
    .then(res => res.json()).then((response) => 
    {
      fetch('https://league-of-stones.herokuapp.com/match/getMatch?token='+this.state.token) // request to wait for the begining of the match
      .then(result => result.json())
      .then((result)=>{
          this.setState({
            matchStart: true
          })
      });
    })
  }
  handleClick(champion) { // handle the selection of the cards
    if (champion.statut === 1 && this.validate === 0){ // remove a card from the deck
      champion.statut = 0;
      this.nombre -= 1;
    }
    else{
      if (this.nombre < 20){ // add a card to the deck
        champion.statut = 1;
        this.nombre += 1;
      }
    }
    this.setState(
      {
        statut: 0
      }
    )
  }
  componentDidMount() { // request and store all the cards of the game in the state "data"
    this._isMounted = true;
    if (this.validate === 1){
    }
    fetch('https://league-of-stones.herokuapp.com/cards/getAll')
    .then(result => result.json())
    .then((result)=>{
      if (this._isMounted) {
        this.setState({
          data: result.data,
        })
      }
    });
  }
  render() {
    fetch('https://league-of-stones.herokuapp.com/match/getMatch?token='+this.state.token)
    .then(result => result.json())
    .then((result)=>{
      if(result.data.player1){
        if(result.data.player1.hand){
          if(result.data.player1.hand > 0|| result.data.player2.hand.length > 0){
            if (this._isMounted) {
              this.setState({
                matchStart: true
              })
            }
          }
        }
      }
    });
    if (this.state.matchStart){
      return <Redirect to='/Match'/>
    }
    const { data } = this.state
    const result = data.map((champion) => {
      if(!(champion.statut === 1)){
        const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
        return <article key={champion.key} onClick={() => this.handleClick(champion)}>
                  <h4>{champion.name}</h4>
                  <img src={url} alt={champion.key}></img>
                  <ul>
                    <li className="attack">{champion.stats.attackdamage}</li>
                    <li className="defence">{champion.stats.armor}</li>
                  </ul>
              </article>
      }
      return "";
    })
    const result2 = data.map((champion) => {
      if(champion.statut === 1){
        const url =  'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + champion.key + '_0.jpg'
        return <article key={champion.key} onClick={() => this.handleClick(champion)}>
                  <h4>{champion.name}</h4>
                  <img src={url} alt="champimg"></img>
                  <ul>
                    <li className="attack">{champion.stats.attackdamage}</li>
                    <li className="defence">{champion.stats.armor}</li>
                  </ul>
              </article>
      }
      return "";
    })
    let msg_help = '';
    let pluriel = 's';
    let pluriel2 = 's';
    if (this.nombre === 0){
      msg_help = 'Cliquez sur les cartes que vous voulez';
    }
    if (this.nombre === 0 ||this.nombre === 1){
      pluriel = '';
    }
    if (20 - this.nombre === 1 || 20-this.nombre === 0){
      pluriel2 = '';
    }
    if (this.nombre < 20){
      return <section>
        <p id="count">Votre deck vous permettra de jouer a League Of Stones, choisissez vos cartes et détruisez vos adversaires<br></br>Vous avez sélectionné {this.nombre} champion{pluriel} sur 20 <b id="compteur_red"><br></br>{20-this.nombre} champion{pluriel2} restant à choisir</b></p>
        <ul>
          <li>Champions disponibles</li>
          <li>Mon deck</li>
        </ul>
        <section id="choice">
          <article id="champion_list"><section>{result}</section></article>
          <article id="deck"><section>{result2}<p id="deck_help">{msg_help}</p></section></article>        
        </section>
        <div id="normal_bg"></div>
        </section>
    }
    else{
      return <section>
        <p id="count">Vous avez choisis vos champions. Vous pouvez maintenant valider votre deck et jouer à League Of Stones!<br></br><button onClick={this.validateDeck}>Valider le deck</button></p>
        <ul>
          <li>Champions disponibles</li>
          <li>Mon deck</li>
        </ul>
        <section id="choice">
          <article id="champion_list"><section>{result}</section></article>
          <article id="deck"><section>{result2}<p id="deck_help">{msg_help}</p></section></article>        
        </section>
        <div id="normal_bg"></div>
      </section>
    }
  }
}
export default Deck;
